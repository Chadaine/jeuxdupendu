﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JeuxDuPendu
{
    public partial class Menu : Form
    {
        private SoundPlayer m_musicPlayer;
        private bool m_soundMuted;
        
        public Menu()
        {
            m_musicPlayer = new SoundPlayer();
            m_musicPlayer.SoundLocation = @"..\..\Resources\Ambiant music.wav";
            m_musicPlayer.PlayLooping();
            m_soundMuted = false;

            InitializeComponent();
            mute.Image = Image.FromFile(@"..\..\Resources\unmuted.png");
        }

        private void Solo_Click(object sender, EventArgs e)
        {
            GameForm game = new GameForm(this, ref m_musicPlayer, ref m_soundMuted);
            game.Show();
            this.Hide();
        }

        private void Exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Multiplayer_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Cette fonctionnalité n'est pas encore disponible.", "Not available");
        }

        private void mute_Click(object sender, EventArgs e)
        {
            if (!m_soundMuted)
            {
                m_soundMuted = true;
                mute.Image = Image.FromFile(@"..\..\Resources\muted.png");
                m_musicPlayer.Stop();
            }
            else
            {
                m_soundMuted = false;
                mute.Image = Image.FromFile(@"..\..\Resources\unmuted.png");
                m_musicPlayer.Play();
            }
        }

        private void form_Activated(object sender, EventArgs e)
        {
            if (!m_soundMuted)
            {
                mute.Image = Image.FromFile(@"..\..\Resources\unmuted.png");
            }
            else
            {
                mute.Image = Image.FromFile(@"..\..\Resources\muted.png");
            }
        }
    }
}
