﻿using System;

namespace JeuxDuPendu
{
    partial class GameForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.bReset = new System.Windows.Forms.Button();
            this.lCrypedWord = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.m_letterUsed = new System.Windows.Forms.Label();
            this.returnButton = new System.Windows.Forms.Button();
            this.mute = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(10, 10);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(256, 256);
            this.panel1.TabIndex = 0;
            // 
            // bReset
            // 
            this.bReset.Font = new System.Drawing.Font("MV Boli", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bReset.Location = new System.Drawing.Point(55, 320);
            this.bReset.Name = "bReset";
            this.bReset.Size = new System.Drawing.Size(145, 35);
            this.bReset.TabIndex = 1;
            this.bReset.Text = "Nouvelle partie";
            this.bReset.UseVisualStyleBackColor = true;
            this.bReset.Click += new System.EventHandler(this.bReset_Click);
            this.bReset.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.bReset_KeyPress);
            // 
            // lCrypedWord
            // 
            this.lCrypedWord.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lCrypedWord.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lCrypedWord.Location = new System.Drawing.Point(10, 275);
            this.lCrypedWord.Name = "lCrypedWord";
            this.lCrypedWord.Size = new System.Drawing.Size(515, 39);
            this.lCrypedWord.TabIndex = 2;
            this.lCrypedWord.Text = "--";
            this.lCrypedWord.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(337, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Lettres déjà utilisées";
            // 
            // m_letterUsed
            // 
            this.m_letterUsed.AutoSize = true;
            this.m_letterUsed.Location = new System.Drawing.Point(286, 56);
            this.m_letterUsed.Name = "m_letterUsed";
            this.m_letterUsed.Size = new System.Drawing.Size(0, 13);
            this.m_letterUsed.TabIndex = 5;
            // 
            // returnButton
            // 
            this.returnButton.Font = new System.Drawing.Font("MV Boli", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.returnButton.Location = new System.Drawing.Point(460, 10);
            this.returnButton.Margin = new System.Windows.Forms.Padding(2);
            this.returnButton.Name = "returnButton";
            this.returnButton.Size = new System.Drawing.Size(70, 25);
            this.returnButton.TabIndex = 6;
            this.returnButton.Text = "Retour";
            this.returnButton.UseVisualStyleBackColor = true;
            this.returnButton.Click += new System.EventHandler(this.returnButton_Click);
            // 
            // mute
            // 
            this.mute.Image = global::JeuxDuPendu.Properties.Resources.unmuted;
            this.mute.Location = new System.Drawing.Point(500, 330);
            this.mute.Name = "mute";
            this.mute.Size = new System.Drawing.Size(30, 25);
            this.mute.TabIndex = 7;
            this.mute.UseVisualStyleBackColor = true;
            this.mute.MouseClick += new System.Windows.Forms.MouseEventHandler(this.mute_MouseClick);
            // 
            // GameForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(539, 361);
            this.ControlBox = false;
            this.Controls.Add(this.mute);
            this.Controls.Add(this.returnButton);
            this.Controls.Add(this.m_letterUsed);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lCrypedWord);
            this.Controls.Add(this.bReset);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "GameForm";
            this.Text = "Jeux du pendu";
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.GameForm_KeyPress);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void letterUsedView_SelectedIndexChanged(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button bReset;
        private System.Windows.Forms.Label lCrypedWord;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label m_letterUsed;
        private System.Windows.Forms.Button returnButton;
        private System.Windows.Forms.Button mute;
    }
}

