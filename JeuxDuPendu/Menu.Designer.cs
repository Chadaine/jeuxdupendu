﻿namespace JeuxDuPendu
{
    partial class Menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.titre = new System.Windows.Forms.Label();
            this.Solo = new System.Windows.Forms.Button();
            this.Multiplayer = new System.Windows.Forms.Button();
            this.Exit = new System.Windows.Forms.Button();
            this.mute = new System.Windows.Forms.Button();
            this.logo = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.logo)).BeginInit();
            this.SuspendLayout();
            // 
            // titre
            // 
            this.titre.Font = new System.Drawing.Font("Matura MT Script Capitals", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.titre.Location = new System.Drawing.Point(4, 252);
            this.titre.Name = "titre";
            this.titre.Size = new System.Drawing.Size(262, 32);
            this.titre.TabIndex = 1;
            this.titre.Text = "Jeux du pendu";
            this.titre.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Solo
            // 
            this.Solo.Font = new System.Drawing.Font("MV Boli", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Solo.Location = new System.Drawing.Point(75, 284);
            this.Solo.Name = "Solo";
            this.Solo.Size = new System.Drawing.Size(112, 41);
            this.Solo.TabIndex = 2;
            this.Solo.Text = "Solo";
            this.Solo.UseVisualStyleBackColor = true;
            this.Solo.Click += new System.EventHandler(this.Solo_Click);
            // 
            // Multiplayer
            // 
            this.Multiplayer.Font = new System.Drawing.Font("MV Boli", 9F, System.Drawing.FontStyle.Bold);
            this.Multiplayer.Location = new System.Drawing.Point(75, 332);
            this.Multiplayer.Name = "Multiplayer";
            this.Multiplayer.Size = new System.Drawing.Size(112, 41);
            this.Multiplayer.TabIndex = 3;
            this.Multiplayer.Text = "Multijoueur";
            this.Multiplayer.UseVisualStyleBackColor = true;
            this.Multiplayer.Click += new System.EventHandler(this.Multiplayer_Click);
            // 
            // Exit
            // 
            this.Exit.Font = new System.Drawing.Font("MV Boli", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Exit.Location = new System.Drawing.Point(75, 379);
            this.Exit.Name = "Exit";
            this.Exit.Size = new System.Drawing.Size(112, 41);
            this.Exit.TabIndex = 4;
            this.Exit.Text = "Quitter";
            this.Exit.UseVisualStyleBackColor = true;
            this.Exit.Click += new System.EventHandler(this.Exit_Click);
            // 
            // mute
            // 
            this.mute.Image = global::JeuxDuPendu.Properties.Resources.unmuted;
            this.mute.Location = new System.Drawing.Point(236, 395);
            this.mute.Name = "mute";
            this.mute.Size = new System.Drawing.Size(30, 25);
            this.mute.TabIndex = 5;
            this.mute.UseVisualStyleBackColor = true;
            this.mute.Click += new System.EventHandler(this.mute_Click);
            // 
            // logo
            // 
            this.logo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.logo.Image = global::JeuxDuPendu.Properties.Resources.logo;
            this.logo.ImageLocation = "";
            this.logo.Location = new System.Drawing.Point(4, 4);
            this.logo.Margin = new System.Windows.Forms.Padding(4);
            this.logo.Name = "logo";
            this.logo.Size = new System.Drawing.Size(263, 244);
            this.logo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.logo.TabIndex = 0;
            this.logo.TabStop = false;
            this.logo.WaitOnLoad = true;
            // 
            // Menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(272, 427);
            this.Controls.Add(this.mute);
            this.Controls.Add(this.Exit);
            this.Controls.Add(this.Multiplayer);
            this.Controls.Add(this.Solo);
            this.Controls.Add(this.titre);
            this.Controls.Add(this.logo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "Menu";
            this.Text = "Menu";
            this.Activated += new System.EventHandler(this.form_Activated);
            ((System.ComponentModel.ISupportInitialize)(this.logo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox logo;
        private System.Windows.Forms.Label titre;
        private System.Windows.Forms.Button Solo;
        private System.Windows.Forms.Button Multiplayer;
        private System.Windows.Forms.Button Exit;
        private System.Windows.Forms.Button mute;
    }
}