﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Media;
using System.Text;
using System.Windows.Forms;
using JeuxDuPendu.MyControls;

namespace JeuxDuPendu
{
    public partial class GameForm : Form
    {

        // Initialisation de l'instance de la classe d'affichage du pendu.
        HangmanViewer _HangmanViewer = new HangmanViewer();
        private string m_randomWord;
        private StringBuilder m_hiddenWord;
        private bool m_letterInWord;
        private Menu m_backToMenu;
        private SoundPlayer m_musicPlayer;
        private bool m_soundMuted;

        /// <summary>
        /// Constructeur du formulaire de jeux
        /// </summary>
        public GameForm(Menu p_backToMenu, ref SoundPlayer p_musicPlayer, ref bool p_soundMuted)
        {
            m_musicPlayer = p_musicPlayer;
            m_soundMuted = p_soundMuted;
            m_backToMenu = p_backToMenu;

            InitializeComponent();
            InitializeMyComponent();
            StartNewGame();
        }

        /// <summary>
        /// Initialisations des composant specifique a l'application
        /// </summary>
        private void InitializeMyComponent()
        {
            // On positionne le controle d'affichage du pendu dans panel1 : 
            panel1.Controls.Add(_HangmanViewer);
			
			// à la position 0,0
            _HangmanViewer.Location = new Point(0, 0);
			
			// et de la même taille que panel1
            _HangmanViewer.Size = panel1.Size;

            // Image par défaut du bouton mute
            if (!m_soundMuted)
            {
                mute.Image = Image.FromFile(@"..\..\Resources\unmuted.png");
            }
            else
            {
                mute.Image = Image.FromFile(@"..\..\Resources\muted.png");
            }
        }

        /// <summary>
        /// Initialise une nouvelle partie
        /// </summary>
        public void StartNewGame()
        {
            m_letterUsed.Text = "";
            m_randomWord = getRandomWord();
            m_hiddenWord = new StringBuilder(getWordUnderscores(), 25);
            m_letterInWord = false;

            // Methode de reinitialisation classe d'affichage du pendu.
            _HangmanViewer.Reset();

            //Affichage du mot à trouver dans le label.
            lCrypedWord.Text = m_hiddenWord.ToString();
        }

        /// <summary>
        /// Méthode appelé lors d'une nouvelle partie afin de prendre un mot aléatoiree
        /// </summary>
        /// <returns></returns>
        public String getRandomWord()
        {
            String[] words = File.ReadAllLines(@"..\..\Resources\WordList.txt");

            Random randomWord = new Random();

            return words[randomWord.Next(words.Length)];
        }

        /// <summary>
        /// Méthode appelé lors d'une nouvelle partie pour convertir le mot a trouvé en underscores
        /// </summary>
        /// <returns></returns>
        public String getWordUnderscores()
        {
            String underscoreWord = "";

            for (int i=0; i<this.m_randomWord.Length;i++)
            {
                underscoreWord += "_";
            }

            return underscoreWord;
        }

        /// <summary>
        /// Methode appelé lors de l'appui d'un touche du clavier, lorsque le focus est sur le bouton "Nouvelle partie"
        /// </summary>
        private void bReset_KeyPress(object sender, KeyPressEventArgs e)
        {
            KeyPressed(e.KeyChar);
        }

        /// <summary>
        /// Methode appelé lors de l'appui d'un touche du clavier, lorsque le focus est sur le formulaire
        /// </summary>
        private void GameForm_KeyPress(object sender, KeyPressEventArgs e)
        {
            KeyPressed(e.KeyChar);
        }

        /// <summary>
        /// Methode appelé lors de l'appui sur le bouton "Nouvelle partie"
        /// </summary>
        private void bReset_Click(object sender, EventArgs e)
        {
            StartNewGame();
        }

        private void KeyPressed(char letter)
        {
            if (m_letterUsed.Text.IndexOf(Char.ToUpper(letter)) == -1)
            {
                m_letterUsed.Text += Char.ToUpper(letter) + ", ";
                m_letterInWord = false;

                for (int i = 0; i < m_randomWord.Length; i++)
                {
                    if (m_randomWord[i] == letter)
                    {
                        m_hiddenWord.Remove(i, 1);
                        m_hiddenWord.Insert(i, Char.ToUpper(letter));
                        lCrypedWord.Text = m_hiddenWord.ToString();

                        m_letterInWord = true;
                    }
                }

                if (m_hiddenWord.ToString().IndexOf("_") == -1)
                {
                    MessageBox.Show("Vous avez gagné !");
                    StartNewGame();
                }
                else if (m_letterInWord == false)
                {
                    // On avance le pendu d'une etape
                    _HangmanViewer.MoveNextStep();

                    // Si le pendu est complet, le joueur à perdu.
                    if (_HangmanViewer.IsGameOver)
                    {
                        MessageBox.Show("Vous avez perdu !");
                        StartNewGame();
                    }
                }
            }
            else
            {
                MessageBox.Show("Lettre déjà utilisé !");
            }
        }

        private void returnButton_Click(object sender, EventArgs e)
        {
            m_backToMenu.Show();
            this.Hide();
        }

        private void mute_MouseClick(object sender, MouseEventArgs e)
        {
            if (!m_soundMuted)
            {
                m_soundMuted = true;
                mute.Image = Image.FromFile(@"..\..\Resources\muted.png");
                m_musicPlayer.Stop();
            }
            else
            {
                m_soundMuted = false;
                mute.Image = Image.FromFile(@"..\..\Resources\unmuted.png");
                m_musicPlayer.Play();
            }
        }
    }

}
